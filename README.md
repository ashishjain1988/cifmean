#CIF-based algorithms for gene-gene network inference
This project was initiated as part of the research on Conditional Inference Forest (CIF) in the context of gene regulatory network inference (GRN). In the heart of the CIF-based algorithms lies the party package developed by [Hothorn et al.](http://eeecon.uibk.ac.at/~zeileis/papers/Hothorn+Hornik+Zeileis-2006.pdf)

##Installation
```R
R CMD INSTALL party_1.0-11.tar.gz
```
