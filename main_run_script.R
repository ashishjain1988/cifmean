#Author: Kirill Bessonov
#Data: May 21 2014
#Description: This script will use modified party package to consturct CI tree and infer gene regulatory network
#The CI tree nodes will be used to find out the predictor variable importance in regulating gene expression Y


explore_forest <- function(ctree_ensemble=NULL){

	ntrees=length(ctree_ensemble);	#total number of trees in ensemble
	
	explore_tree <- function(ctree=NULL){
	  forest_tree_nodes_num[ntree] <<- forest_tree_nodes_num[ntree]+1
	  
	  if(ctree$terminal == FALSE | ctree$nodeID == 1){
		node_obs <- ctree$sumweights
		node_stats <- ctree$criterion$criterion #p-value 
		#node_stats <- ctree$criterion$statistic #test stat
		
		if(ctree$terminal == FALSE){
			node_varIdx <- ctree$psplit$variableID
			n_stat <- node_stats[node_varIdx] #node feature statistic or p-value
			#cat("Variable: ",ctree$psplit$variableName[[1]],"\n")
		}else{ #in case node is terminal and tree has only 1 node
			node_varIdx <- which.max(node_stats)[[1]]
			if(any(is.na(node_stats)==T)){cat("NA value")}
			n_stat <- max(node_stats, na.rm=T)
			#cat("\nVariable: ", names(node_stats)[node_varIdx]," and n_stat=", n_stat ,"\n")
		}
		#cat("Node #obs:", node_obs, "Cselect=",n_stat," pval=",ctree$criterion$maxcriterion,"\n"); 

		
		#recursion
		#if(1){
		#populate the predict X vector with test stat or p-value
		#list_genes_stats = global variable
		#select values based on number of observations in the node (more observ --> more trust in the value)
		if(list_genes_stats[node_varIdx] < n_stat){ 
			if (list_genes_obs[node_varIdx] < node_obs){
				#cat ("current=",list_genes_obs[node_varIdx], " and candidate=", node_obs , "\n")
				#if(n_stat==1.0){n_stat=0.999}
				list_genes_stats[node_varIdx] <<- n_stat; #p-value (the larger the more significant) 
				list_genes_obs[node_varIdx] <<- node_obs;
			}#if inner
		}#if outer

		left_node<-ctree$left; #move to the left node
		if(is.null(left_node)){return(0)}
		if(left_node$terminal==F ){
			#left_stat <- get_node_stat(left_node);
			explore_tree(left_node);
		}

		right_node<-ctree$right;#move to the right node
		if(is.null(right_node)){return(0)}
		if(right_node$terminal == F){
			#right_stat <- get_node_stat(right_node);
			explore_tree(right_node)
		} 
		#}#if temp

	} 

	}#explore_tree() end

	ntree=1; #counter
	xnames=names(cforest_obj@data@env$input); 
	ntrees_stats=matrix(0,ncol=length(xnames),nrow=ntrees, dim=list(c(),xnames) ); 
	ntrees_node_counts=matrix(0,ncol=length(xnames),nrow=ntrees, dim=list(c(),xnames) ); 
	forest_tree_nodes_num <- rep(0, length(ctree_ensemble));
	#print(dim(ntrees_stats))
	
	for(ctree in ctree_ensemble){	
		#cat("Processing tree ",ntree," out of ", ntrees," \n");	
		#global variables
		list_genes_stats <- rep(0,length(xnames)); names(list_genes_stats)<-xnames #stores test stats for predictors
		list_genes_obs <- rep(0,length(xnames)); names(list_genes_obs)<-xnames #stores number of observations per node
		
		explore_tree(ctree);

		#print(list_genes_stats)
		#print(list_genes_obs)
		#print(class(list_genes_stats))
		#print(length(ntrees_stats[ntree,]))
		
		ntrees_stats[ntree,] <- list_genes_stats;
		ntrees_node_counts[ntree,] <- list_genes_obs;
		ntree=ntree+1;
	}
  
    
	return(list(stats=ntrees_stats,obs_counts=ntrees_node_counts, num_nodes=forest_tree_nodes_num));
}



#populates adjacency matrix using set of ensemble CI trees
#Amtx adjacency matrix to fill out
#Amtx_row = name of the gene --> row
populateAmtx <- function(cf_res=NULL, mode=NULL, Amtx=NULL, Amtx_row=NULL){
	
	if (mode=="maxCounts"){
		for(gene in colnames(cf_res$stats)){
			max_cts_idx = which.max(cf_res$counts[,gene]);
			pval_gene = cf_res$stats[max_cts_idx,gene];
			#cat("gene=",gene,"  pvals=",pval_gene," \n");
			Amtx[Amtx_row,gene]=pval_gene
			#print(Amtx[Amtx_row,gene])
		}#for
		return(Amtx);

	} else if (mode=="weight_mean"){
		sum_stats <- apply(cf_res$stats,2,sum) #sum of p-vals per gene from [trees x genes] matrix
		#sel_genes <- names(which(sum_stats > quantile(sum_stats, 0.3)))
		for(gene in names(sum_stats)){
			w_means <- weighted.mean(cf_res$stats[,gene], cf_res$counts[,gene])
			if(is.na(w_means)){ w_means=0 }
		
			#FILTER 2: via quantiles
			w_means[which(w_means< quantile(w_means,0.25) ) ] = 0;
			#if (w_means < 0.90){w_means = 0;} #extra_filter for accuracy
			Amtx[Amtx_row, gene] = w_means;	 
		}
		return(Amtx)

	} else if (mode=="mean"){  #increases PR
		sum_stats <- apply(cf_res$stats,2,sum) #sum of p-vals per gene from [trees x genes] matrix
		#sel_genes <- names(which(sum_stats > quantile(sum_stats, 0.3)))
		nz_vals <- apply(cf_res$stats, 2, function (x) { length(which(x>0)) } )
		#for(gene in sel_genes){	
		#	nz_vals <- length(which(stats_ntrees$stats[,gene]>0))
			#cat(gene," ", sum_stats[gene]/nz_vals,"\n")
		#	Amtx[Amtx_row, gene] = sum_stats[gene]/nz_vals
		#}
		means <- sum_stats/nz_vals;  means=means[!is.na(means)]
		
		#FILTER 1: extra filter to increase precision 
		#means[which(means<0.95)]=0

		#FILTER 2: via quantiles
		#means[which(means< quantile(means,0.25) ) ] = 0
		
		#qt = quantile(means, 0.95)
		#sel_means <- means[means > qt]
		Amtx[Amtx_row, names(means)] = means;
	
		return(Amtx);

	} else if (mode=="quantile"){ cat("Quantile method\n")
		sum_stats <- apply(cf_res$stats,2,sum) #sum of p-vals per gene from [trees x genes] matrix
		sel_genes <- names(which(sum_stats > quantile(sum_stats, 0.95))) #select genes that are within 5% extreeme of the stats sums
		sum_stats <- apply(cf_res$stats[,sel_genes],2,max) #look at [trees x genes] matrix and get max p-value for sel. genes
		#filter2 of significace
		#sum_stats[which(sum_stats < 0.95)] = 0 

		nz_stats <- sum_stats[which(sum_stats > 0)]
		

		#idx_replace <- mapply( function(x,y){x>y}, nz_stats, Amtx[Amtx_row,names(nz_stats)] ) 
		#print(nz_max_stats[idx_replace])
		#Amtx[Amtx_row, names(nz_stats)[idx_replace]] = nz_stats[idx_replace]; 
		
		
		#print(Amtx[Amtx_row,])
		Amtx[Amtx_row,names(nz_stats)] = nz_stats
		return(Amtx);
	} else if (mode=="sum"){
		sum_stats <- apply(cf_res$stats,2,sum) #sum of p-vals per gene from [trees x genes] matrix
		#sum_stats[which(sum_stats < 0.95)] = 0; #filter of significance
		nz_stats <- sum_stats[which(sum_stats > 0)];
		Amtx[Amtx_row,names(nz_stats)] = nz_stats
		return(Amtx);
	} else if (mode == "FisherCombined"){ #use classical Fisher's method based on Chi distribution
		idx_nz = apply(cf_res$stats, 2, function(x){ which(x>0) }); df <- lapply(idx_nz, function(x){length(x)*2})
		
		#lapply(names(idx_nz), function(g){ -2*sum(log(stats_ntrees$stats[idx_nz[[g]],g])) })
		
		Amtx[Amtx_row,names(idx_nz)] =  unlist( lapply(names(idx_nz), function(g){ pchisq(-2*sum(log(cf_res$stats[idx_nz[[g]],g])), 
									df=df[[g]], lower.tail=F)  }) )
		print(Amtx)
		return(Amtx)
	} else if (mode == "Bonferroni") { #Bonferroni based aggregation of p-values (the large p-values are most significant)
		idx_nz = apply(cf_res$stats, 2, function(x){ which(x>0) }); n_trees <- lapply(idx_nz, function(x){length(x)*2})
		max_p_vals = unlist( lapply(names(idx_nz), function(g){ max(cf_res$stats[idx_nz[[g]],g])/n_trees[[g]] } ) )
		Amtx[Amtx_row,names(idx_nz)] = max_p_vals
		return(Amtx)

	} else {stop("Wrong aggregation method name")}
	
	
}

get_source_data <- function(in_file=""){
	cat("Reading input data ...\n")
	
	cat("Input file ", in_file,"\n")
	expr_data <- read.table(file=in_file, sep="\t", header=T)
	return(expr_data)
}

record_paramters <- function(controls = NULL){
	params_print <- list()
	cat("\n",rep("*",15), "\n")
	cat("TREE GROWTH CONTROL PARAMS:\n")
	params_print$teststat<- as.character(controls@varctrl@teststat); cat("Test stat type: ",params_print$teststat,"\n");
	params_print$testtype<- as.character(controls@gtctrl@testtype); cat("Mult-test correction: ",params_print$testtype,"\n")
	params_print$maxdepth<- as.character(controls@tgctrl@maxdepth);cat("Max depth: ",params_print$maxdepth, "\n")
	params_print$minsplit<- as.character(controls@splitctrl@minsplit) ;cat("Min split: ",params_print$minsplit, " obs.\n")
	params_print$mincriterion <- as.character(controls@gtctrl@mincriterion);cat("mincriterion: ",params_print$mincriterion, " (1-pval) \n")
	params_print$mtry <- as.character(controls@gtctrl@mtry); cat("Mtry: ",params_print$mtry, "\n")
	params_print$fraction <- as.character(controls@fraction); cat("Fraction: ",params_print$fraction, "\n")
	params_print$ntrees <- as.character(controls@ntree); cat("Ntrees: ", params_print$ntrees , "\n")
	cat(rep("*",15), "\n")
	return(params_print)
}

#convert matrix to pairwise list (3 columns)
matrix2pwList = function (mtx)
{
    if (is.matrix(mtx) == F) {
        stop("The input is not of matrix type\n")
    }
	
	mtx = t(mtx) #since R reads matrix column by column
    from.genes <- rownames(mtx)
    to.genes <- colnames(mtx)
    dim_m = dim(mtx)[1] * dim(mtx)[2]
    pwList <- data.frame(matrix(0, ncol = 3, nrow = dim_m))
    colnames(pwList) = c("from.gene", "to.gene", "edge_weight")
    pwList[, 1] =  rep(from.genes, dim(mtx)[2])  
    pwList[, 2] =  rep(to.genes, each = dim(mtx)[1])
    pwList[, 3] = as.numeric(mtx)
	
    pwList=pwList[which(pwList[,3]>0), ]
    return(pwList)
}




#------------------------MAIN ()--------------------------------------------------------------------
set.seed(2014) #IMPORTANT set seed to improve reproducibility of results
lapply(c("party","gtools"), require, character.only=T) #apply require() to the character vector
#Sys.sleep(10);
#if( length(grep("predictions",list.dirs("../")) )  ==  0 | length(grep("saves",list.dirs("../")) ) ==  0 ){
#		stop("\n\n\t\"Missing PREDICIONS and SAVES directories!\"")}

 
#transcription factors

for(net_num in c(4) ){
  cat("***Calculations on NET#", net_num ,"***\n")
 
  prefix1="D5_Network";
  root_dir=paste("/scratch/kbessono/MANUSC_PROD_RUNS_J14/DATA/")
  tf_file= paste(root_dir,prefix1,net_num,"_transcription_factors.tsv",sep="")
  TFs = as.character(unlist(read.table(file=tf_file, sep="\t", header=F, colClasses="character") ))
 
  #!!!!RUNS LOOP START
  for(run_number in c(1) ){
   cat("***Performing RUN#", run_number ,"***\n")


#DREAM5
 prefix2="_expression_data.tsv"
in_file_expr <- paste(root_dir,prefix1,net_num,prefix2,sep="")

cat("Input file:",in_file_expr,"\n")
expr_data <- get_source_data(in_file_expr)


var_names <- names(expr_data)
Amtx <- matrix(0,nrow=length(var_names), ncol=length(var_names), dimname=list(var_names, var_names)) 

total_obs=dim(expr_data)[1]
total_genes=dim(expr_data)[2]

#**********************************************************************
#CONFIGURATION of the CI tree growing algorithm. Introduce parameters
controls=cforest_control(teststat="quad", testtype="Univariate",  fraction=0.623, replace=F,
							mincriterion=0.95, minsplit=20, ntree=1000, mtry=round(length(TFs)/3), maxdepth=0,savesplitstats = F)
controls@splitctrl@minbucket=10 #min observations req to be in a node
controls@splitctrl@minprob=0.00001 #controls maximum number of observations in the node
#**********************************************************************

parameters <- record_paramters(controls); #record CIF paramters into list
parameters$inputFile <- in_file_expr; #input file name



#Feature ranking analysis
#Sys.sleep(100);
gene_counter=1;  var_imp_lists <- list()
time_start = as.numeric(Sys.time()) #initial time
list_ntree_nodes = list()

for(gene_name in var_names){
	if(gene_counter %% 1 ==0){
		cat("\n***ANALYZING gene ", gene_name," (",gene_counter," out of ",total_genes,")...\n"); flush.console();
	}
	time_end = as.numeric(Sys.time())
	print(Sys.time())
	time_total = time_end - time_start
	#cat("Processing time elapsed: ",time_total, "s,",time_total/60," min\n")
	#data selection (TF--> gene)
	selected_v = unique( c(gene_name,TFs) )	

	#build a CI for each gene using formula for each gene (all vs one) 
	cforest_obj = cforest(formula(paste(gene_name," ~.", sep="")), xtrafo=0, data = expr_data[selected_v], control=controls) 
	stats_ntrees <- explore_forest(cforest_obj@ensemble);
	#stats_ntrees$stats[,1] = rep(1,100)
	#update adjacency matrix  MinBonferroni FisherCombined
	Amtx <- populateAmtx(stats_ntrees, mode="mean", Amtx, gene_name);
	if(length(Amtx) == 0 ){stop("Amtx damaged")}
		
	#free up memory
	if(gene_counter < 10){
	  list_ntree_nodes =c(list_ntree_nodes,stats_ntrees$num_nodes)
	}
	
	rm(cforest_obj, stats_ntrees)
	gc(reset=T);

	gene_counter=gene_counter+1; 
}
#sorted pwlist of gene gene interactions generated here

#======END=====
pwl = matrix2pwList(Amtx); 

sort_idx = sort(pwl[,3], decreasing=T, index.return=T)$ix; 
pwl=pwl[sort_idx,]; pw_file_name = paste("D5_N",net_num,"_Run", run_number,".txt", sep="")
#write sorted PW list for DREAM scripts evalutation
write.table(pwl, file=paste("./predictions/", pw_file_name, sep=""), sep="\t", row.names=F, col.names=F, quote=F)

#Time taken
time_end = as.numeric(Sys.time())
time_total = time_end - time_start
cat("ELAPSED TIME:  ",time_total, "s,",time_total/60," min\n")
	

#save WS desktop just in case
save.image( file=paste("./saves/",substr(pw_file_name,1,nchar(pw_file_name)-4) , ".Rdata", sep=""), compress=T)
cat("Image saved\n")

}#for net
}#for run



